/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.korn.Databaseproject.dao.ProductDao;
import com.korn.Databaseproject.dao.ReceiptDao;
import com.korn.Databaseproject.dao.ReceiptDetailDao;
import com.korn.Databaseproject.model.Product;
import com.korn.Databaseproject.model.Receipt;
import com.korn.Databaseproject.model.ReceiptDetail;

import java.util.List;

/**
 *
 * @author Good
 */
public class TestReceiptDetailDao {
    public static void main(String[] args) {
        ReceiptDetailDao rdd = new ReceiptDetailDao();
        for(ReceiptDetail rd: rdd.getAll()){
            System.out.println(rd);
        }
        ReceiptDao rd = new ReceiptDao();
        ProductDao pd = new ProductDao();
        List<Product> products = pd.getAll();
        Product product0 = products.get(0);
        Receipt receipt = rd.get(1);
        ReceiptDetail newReceiptDetail = new ReceiptDetail(product0.getId(), product0.getProductName(), product0.getProductPrice(), 1, product0.getProductPrice()*1,receipt.getId());
        rdd.save(newReceiptDetail);
    }
}
