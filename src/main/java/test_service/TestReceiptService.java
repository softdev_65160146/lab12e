/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.korn.Databaseproject.dao.ProductDao;
import com.korn.Databaseproject.model.Product;
import com.korn.Databaseproject.model.Receipt;
import com.korn.Databaseproject.model.ReceiptDetail;
import com.korn.Databaseproject.service.ReceiptService;
import java.util.List;

/**
 *
 * @author Good
 */
public class TestReceiptService {
    public static void main(String[] args) {
        ReceiptService receiptService = new ReceiptService();
        for(Receipt receipt :receiptService.getReceipts()){
            System.out.println(receipt);
        }
        System.out.println(receiptService.getById(1));
        Receipt r1 = new Receipt( 1000, 10, 1);
        ProductDao pd = new ProductDao();
        List<Product> products = pd.getAll();
        Product product0 = products.get(0);
        Product product1 = products.get(1);
        
        ReceiptDetail newReceiptDetail1 = new ReceiptDetail(product0.getId(), product0.getProductName(), product0.getProductPrice(), 1, product0.getProductPrice()*1,-1);
        r1.addReceiptDetail(newReceiptDetail1);
        
        
        ReceiptDetail newReceiptDetail2 = new ReceiptDetail(product1.getId(), product1.getProductName(), product1.getProductPrice(),2 , product1.getProductPrice()*2,-1);
        r1.addReceiptDetail(newReceiptDetail2);
        System.out.println(r1);
        receiptService.addNew(r1);
        
       // for(Receipt receipt :receiptService.getReceipts()){
       //     System.out.println(receipt);
        }
    }
    

    
  