/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.korn.Databaseproject.service;

import com.korn.Databaseproject.dao.ProductDao;
import com.korn.Databaseproject.model.Product;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ProductService {
    public Product getById(int id){
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }

    public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
}
