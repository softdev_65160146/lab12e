/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.korn.Databaseproject.service;

import com.korn.Databaseproject.dao.ReceiptDetailDao;
import com.korn.Databaseproject.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDetailService {
    public ReceiptDetail getById(int id){
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.get(id);
    }

    public List<ReceiptDetail> getReceiptDetails(){
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.getAll(" receipt_detail_id asc");
    }

    public ReceiptDetail addNew(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.save(editedReceiptDetail);
    }

    public ReceiptDetail update(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.update(editedReceiptDetail);
    }

    public int delete(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.delete(editedReceiptDetail);
    }
}
